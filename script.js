const troca_cor = () => {
    let cor_selecionada = document.getElementById('color-input').value;
    document.getElementById('colorida').style.backgroundColor = cor_selecionada;
}

document.getElementById('color-input').addEventListener('change', troca_cor)


//QUANTIDADE
let montadoras = ['BMW', 'Porsche', 'Volkswagen', 'Chevrolet', 'Fiat', 'Audi']
let qtdmax_montadoras = document.getElementById('qtd-montadoras')


let montadoras_select = document.getElementById('montadoras-select')

qtdmax_montadoras.max = montadoras.length
qtdmax_montadoras.min = 0;
qtdmax_montadoras.style.width = '150px';

const muda_qtd_montadoras = () => {
    montadoras_select.innerHTML = ""

    for(let i = 1; i <= qtdmax_montadoras.value; i++){
        montadoras_select.options[i-1] = new Option(montadoras[i-1])
       //limitar os options apenas para a qtdmax_montadoras
    }

}

qtdmax_montadoras.addEventListener('change', muda_qtd_montadoras)


//VER MONTADORA
let montadora_selecionada = document.getElementById('montadora-selecionada')


const muda_montadora = () => {
    let option_selected = montadoras_select.value
    montadora_selecionada.innerText = option_selected
   
}

montadoras_select.addEventListener('change', muda_montadora)

// NOVA MONTADORA
const bt_add_montadora = document.getElementById('bt-add-montadora')
let nova_montadora = document.getElementById('nova-montadora')

const add_montadora = () => {
    montadoras.push(nova_montadora.value)
    qtdmax_montadoras.max = montadoras.length
}

bt_add_montadora.addEventListener('click', add_montadora)
bt_add_montadora.addEventListener('click', muda_qtd_montadoras)


//'DELETAR MONTADORA
const bt_remove_montadora = document.getElementById('bt-remove-montadora')

const remove_montadora = () => {
    montadoras.pop()
    console.log(montadoras)
}

bt_remove_montadora.addEventListener('click', remove_montadora)
bt_remove_montadora.addEventListener('click', muda_qtd_montadoras)
